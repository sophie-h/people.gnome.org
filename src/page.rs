use askama::Template;
use page_tools::{Apps, People, Site};

// used in templates
use crate::filters;
use std::ops::Deref;

#[derive(Template)]
#[template(path = "index.html")]
pub struct Index<'a> {
    pub site: &'a Site,
    pub people: &'a People,
    pub apps: &'a Apps,
}
