use page_tools::output::{self, Dir, Output, OutputSimple, Scss};
use page_tools::{Apps, Language, People, Site};

use crate::page;

pub fn generate() -> color_eyre::Result<()> {
    output::prepare();

    let people = People::from_upstream_cached().unwrap().shuffle();
    let apps = Apps::from_upstream_cached().unwrap();

    Dir::from("assets/").output("assets/");

    Scss::from("scss/main.scss").output("assets/style/main.css");

    let site = Site::new(&Language::en());

    let overview = page::Index {
        site: &site,
        people: &people,
        apps: &apps,
    };
    overview.output("index.html");

    output::complete()?;

    Ok(())
}
