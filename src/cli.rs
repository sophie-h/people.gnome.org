use crate::generate;

pub fn main() {
    env_logger::init_from_env(
        env_logger::Env::default().filter_or(env_logger::DEFAULT_FILTER_ENV, "info"),
    );

    color_eyre::install().unwrap();

    generate::generate().unwrap();
}
